package renatsayf.examplepopupservice;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

// TODO: 3 - Создаем класс  ServiceDialog extends AppCompatActivity

public class ServiceDialog extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.service_dialog_activity);

        ModalFragment modalFragment = ModalFragment.newInstance(null, null);
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_frame, modalFragment).commit();
    }
}
