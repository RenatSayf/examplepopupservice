package renatsayf.examplepopupservice;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;

import java.lang.ref.WeakReference;

// TODO: 2 - Создаем класс ServiceHandler extends Handler

public class ServiceHandler extends Handler
{
    private WeakReference<MyService> wrActivity;

    public ServiceHandler(MyService activity)
    {
        wrActivity = new WeakReference<>(activity);
    }

    @Override
    public void handleMessage(Message msg)
    {
        super.handleMessage(msg);
        MyService activity = wrActivity.get();
        if (activity != null)
        {
            showActivity(activity);
        }
    }

    private void showActivity(MyService activity)
    {
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.setClass(activity, ServiceDialog.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("text", "Hello!");
        activity.startActivity(intent);
    }
}
