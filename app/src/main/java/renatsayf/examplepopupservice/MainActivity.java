package renatsayf.examplepopupservice;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity
{
    public static Intent serviceIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        if (serviceIntent != null)
        {
            stopService(serviceIntent);
        }
    }

    @Override
    public void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        serviceIntent = new Intent(this, MyService.class);
        startService(serviceIntent);
    }
}
